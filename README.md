# QEMU AAOS

Script to start Android Automotive OS with upstream QEMU

## Usage

### Help page

```bash
usage: ./qemu-aaos.sh [OPTION...]

Lunch AAOS in a QEMU/KVM VM

 --dry-run         print QEMU cmdline without launch the VM

AAOS
 --images          AAOS images path [def. ./images]

QEMU
 --qemu            QEMU binary path [def. qemu-system-x86_64]
 --extra           QEMU extra cmdline

VM settings
 --mem             VM memory [def. 4G]
 --smp             VM vCPUs [def. 4]

 -h, --help        print this help
```
