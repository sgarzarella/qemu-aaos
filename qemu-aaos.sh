#!/bin/bash

function usage
{
    echo -e "usage: $0 [OPTION...]"
    echo -e ""
    echo -e "Lunch AAOS in a QEMU/KVM VM"
    echo -e ""
    echo -e " --dry-run         print QEMU cmdline without launch the VM"
    echo -e ""
    echo -e "AAOS"
    echo -e " --images          AAOS images path [def. ${AAOS_IMG}]"
    echo -e ""
    echo -e "QEMU"
    echo -e " --qemu            QEMU binary path [def. ${QEMU}]"
    echo -e " --extra           QEMU extra cmdline"
    echo -e ""
    echo -e "VM settings"
    echo -e " --mem             VM memory [def. ${MEM}]"
    echo -e " --smp             VM vCPUs [def. ${SMP}]"
    echo -e ""
    echo -e " -h, --help        print this help"
}

DRY_RUN=0

QEMU="qemu-system-x86_64"
AAOS_IMG="./images"

#MACHINE="q35,accel=kvm"
MACHINE="pc-i440fx-6.2,accel=kvm"
CPU="host"
MEM="8G"
SMP="4"

IOTHREAD_OBJ="iothread,id=disk-iothread"
IOTHREAD_DEV="iothread=disk-iothread"

APPEND="no_timer_check 8250.nr_uarts=1 clocksource=pit console=hvc0 \
cma=266M@0-4G printk.devkmsg=on bootconfig \
debug drm.debug=0x0 androidboot.selinux=permissive enforcing=0"
# enforcing=0 androidboot.selinux=0"

HVC0=("-chardev" "stdio,id=forhvc0" \
    "-device" "virtio-serial-pci,ioeventfd=off" \
    "-device" "virtconsole,chardev=forhvc0")
HVC1=("-chardev" "null,id=forhvc1" \
    "-device" "virtio-serial-pci,ioeventfd=off" \
    "-device" "virtconsole,chardev=forhvc1")

VIDEO=("-device" "virtio-gpu-gl-pci" "-display" "gtk,gl=on")
#VIDEO=("-vga" "virtio" "-display" "gtk,gl=on")

AUDIO=("-device" "intel-hda" "-device" "hda-duplex")

NIC=("-nic" "user,model=virtio")
#NIC=("-netdev" "user,id=mynet,hostfwd=tcp::5550-:5555" "-device" "virtio-net-pci,netdev=mynet")

INPUT=("-device" "virtio-tablet-pci" "-usbdevice" "mouse" "-usbdevice" "keyboard")
#INPUT=("-device" "virtio-mouse-pci" "-device" "virtio-keyboard-pci")

VSOCK=("-device" "vhost-vsock-pci,guest-cid=77")

MISC=("-no-user-config" "-nodefaults" "-d" "guest_errors")

#EXTRA="-device nec-usb-xhci,id=xhci -device sdhci-pci"

while [ "$1" != "" ]; do
    case $1 in
        --dry-run )
            DRY_RUN=1
            ;;
        --extra )
            shift
            EXTRA=$1
            ;;
        --images )
            shift
            AAOS_IMG=$1
            ;;
        --mem )
            shift
            MEM=$1
            ;;
        --qemu )
            shift
            QEMU=$1
            ;;
        --smp )
            shift
            SMP=$1
            ;;
        -h | --help )
            usage
            exit
            ;;
        * )
            echo -e "\nParameter not found: $1\n"
            usage
            exit 1
    esac
    shift
done


# Images

KERNEL_IMG="${AAOS_IMG}/kernel-ranchu"
INITRD_IMG="${AAOS_IMG}/initrd"
SYSTEM_IMG="${AAOS_IMG}/system-qemu.img"
CACHE_IMG="${AAOS_IMG}/cache.img.qcow2"
USERDATA_IMG="${AAOS_IMG}/userdata-qemu.img.qcow2"
ENCRYPT_IMG="${AAOS_IMG}/encryptionkey.img.qcow2"
VENDOR_IMG="${AAOS_IMG}/vendor-qemu.img"


# Block Drives

BLK_DRV_RO="if=none,read-only=on"
BLK_DRV_RW="if=none,overlap-check=none,cache=unsafe,l2-cache-size=1048576"
BLK_DEV="virtio-blk-pci,modern-pio-notify=on,${IOTHREAD_DEV}"

SYSTEM_BLK=(-drive "${BLK_DRV_RO},index=0,id=system,file=${SYSTEM_IMG}" \
    -device "${BLK_DEV},drive=system,bus=pci.0,addr=0x3")
CACHE_BLK=(-drive "${BLK_DRV_RW},index=1,id=cache,file=${CACHE_IMG}" \
    -device "${BLK_DEV},drive=cache,bus=pci.0,addr=0x4")
USERDATA_BLK=(-drive "${BLK_DRV_RW},index=2,id=userdata,file=${USERDATA_IMG}" \
    -device "${BLK_DEV},drive=userdata,bus=pci.0,addr=0x5")
ENCRYPT_BLK=(-drive "${BLK_DRV_RW},index=3,id=encrypt,file=${ENCRYPT_IMG}" \
    -device "${BLK_DEV},drive=encrypt,bus=pci.0,addr=0x6")
VENDOR_BLK=(-drive "${BLK_DRV_RO},index=4,id=vendor,file=${VENDOR_IMG}" \
    -device "${BLK_DEV},drive=vendor,bus=pci.0,addr=0x7")

DRIVERS=("${SYSTEM_BLK[@]}" "${CACHE_BLK[@]}" "${USERDATA_BLK[@]}" \
    "${ENCRYPT_BLK[@]}" "${VENDOR_BLK[@]}")


# QEMU cmdline

EXEC=("${QEMU}" "${MISC[@]}" -machine "${MACHINE}" -cpu "${CPU}" \
    -m "${MEM}" -smp "${SMP}" -object "${IOTHREAD_OBJ}" \
    -kernel "${KERNEL_IMG}" -initrd "${INITRD_IMG}" "${DRIVERS[@]}" \
    "${NIC[@]}" "${HVC0[@]}" "${HVC1[@]}" "${INPUT[@]}" \
    "${VIDEO[@]}" "${AUDIO[@]}" "${VSOCK[@]}" -append "${APPEND}")


if [ "${DRY_RUN}" == "1" ]; then
    echo "${EXEC[@]}" ${EXTRA}
    exit
fi

set -e -x

exec "${EXEC[@]}" ${EXTRA}
